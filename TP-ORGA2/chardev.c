#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>

struct fake_device {
    char data[100];
    struct semaphore sem;
} virtual_device;

struct cdev *mcdev;
int major_number;
int ret;

dev_t dev_num;

#define DEVICE_NAME "chardevice"


int device_open (struct inode *inode, struct file *file){
    if(down_interruptible(&virtual_device.sem)!=0){
        printk(KERN_ALERT "UNGS No se bloqueo el device_open");
        return -1;
    }
    printk(KERN_INFO "UNGS: Open device");
    return 0;
}

ssize_t device_read (struct file *filp, char *buffer, size_t length, loff_t *offset){
    printk(KERN_INFO "UNGS: Reading from device");
    ret=copy_to_user (buffer,virtual_device.data,length);
    return ret;
}

ssize_t device_write (struct file *filp, const char *buffer, size_t length, loff_t *offset){
    printk(KERN_INFO "UNGS: Writing to device");
    ret=copy_from_user(virtual_device.data,buffer,length);
    return ret;
}

int device_release (struct inode *inode, struct file *filp){
    up(&virtual_device.sem);
    printk(KERN_INFO "UNGS: Close device");
    return 0;
}

struct file_operations fops = {
    .owner=THIS_MODULE,
    .open = device_open,
    .release = device_release,
    .write = device_write,
    .read = device_read
};

static int driver_entry (void){
    ret =alloc_chrdev_region(&dev_num,0,1,DEVICE_NAME);
    if (ret <0){
        printk(KERN_ALERT "UNGS: fNo se pudo asignar un major number");
        return ret;
    }
    major_number=MAJOR(dev_num);
    printk(KERN_INFO "UNGS: Tengo major number %d\n", major_number);
    printk(KERN_INFO "crear un device file con\n");
    printk(KERN_INFO "\tuse \"mknod /dev/%s c %d 0\"", DEVICE_NAME, major_number);
    mcdev=cdev_alloc();
    mcdev->ops=&fops;
    mcdev->owner=THIS_MODULE;
    ret=cdev_add(mcdev,dev_num,1);
    if (ret <0){
        printk(KERN_ALERT "UNGS: No se pudo agregar cdev al kernel");
        return ret;
    }
    sema_init(&virtual_device.sem,1);
    return 0;
}

static void driver_exit (void){
    cdev_del(mcdev);
    unregister_chrdev_region (dev_num,1);
    printk(KERN_ALERT "UNGS: Unregister char device: %d\n", ret);
}

module_init(driver_entry);
module_exit(driver_exit);
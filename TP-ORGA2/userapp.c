#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#define DEVICE "/dev/chardevice"

int main(){
    int i,fd;
    char ch, write_buf[100],read_buf[100];
    fd=open(DEVICE,O_RDWR);
    if(fd ==-1){
        printf("El file %s no existe o lo ha bloqueado otro proceso\n",DEVICE);
        exit(-1);
    }
    printf("r = Leer del device\nw = Escribir en el device\ningrese comando: ");
    scanf ("%c",&ch);

    switch (ch){
        case 'w':
                printf("Ingrese datos: ");
                scanf(" %[^\n]",write_buf);
                write(fd, write_buf, sizeof(write_buf));
                break;
        case 'r':
                read(fd, read_buf,sizeof(read_buf));
                printf("Device: %s\n",read_buf);
                break;
        default:
                printf("Comando no reconocido\n");
                break;
    }
    close(fd);

    return 0;
}